#include<stdio.h>
#include <time.h>
#include <stdlib.h>

int main() {

	FILE *f = fopen("input.txt", "w");
	int n ;
	printf("N = ");
	scanf("%d",&n);

	fprintf(f, "%d\n", n);
	int i, j;
	for (i = 0; i < n; i++ ) {
		for (j = 0; j < n; j++) {
			int r = 0;
			if (i != j) {   
				r = rand() % 50; 
			}
			fprintf(f, "%d\n", r);
		}
	}
	fclose(f);
	return 1;
}
