### MC723 - Laboratório de Projeto de Sistemas Computacionais<p>Nome: Bleno Humberto Claus<br>Ra: 145444<p>Daniel Ricci<br>Ra: 14782<p>
------------------

# Relatório Projeto 1 - Parte 1

**Escolha do Programa: O que faz? Para que serve?**

O programa escolhido foi o do TSP (Travelling Salesman Problem, ou Problema do Caxeiro Viajante), que consiste em, dado um grafo com custos associados à suas arestas, encontrar o caminho de menor custo que, partindo de um vértice inicial, percorre todos os outros vértices uma única vez (sem repetição de arestas) e retorna ao vértice de ínicio. Esse problema foi inspirado na necessidade de vendedores em realizar entregas em várias cidades, percorrendo o menor caminho possível, reduzindo o tempo e os custos da viagem.

**Por que é bom para medir desempenho?**

Devido à sua complexidade (NP-Completo) computacional e sua alta utilização de recursos, como memória e processamento. Na implementação usada nesse projeto (retirada [daqui](https://www.thecrazyprogrammer.com/2017/05/travelling-salesman-problem.html)), a complexidade é O(n²2^n), conforme explicado no próprio link. A estratégia usada nesse caso é a programação dinâmica, e há também a complexidade de espaço, que é O(n2^n). Portanto, devido à sua alta complexidade, com uma entrada suficientemente grande, ele é bom para medir não apenas o desempenho do processador, mas também do disco.

**O que baixar?**

O código fonte se encontra nesse repositório (tsp.c).

**Como compilar/instalar?**

Também se encontra nesse repositório um Makefile para compilar o programa. Então basta usar o comando *make* para gerar um executável chamado tsp.

**Como executar?**

Basta rodar o executável gerado pelo *make*. A entrada esperada para esse programa é composta pelo número *n* de cidades, seguido pela matriz de adjacência *n x n* com os custos de viajar entre as cidades. Caso não haja um caminho direto entre duas cidades, isso deve ser representado por um valor muito alto na matriz. Uma entrada de exemplo está contida nesse repositório, chamadas de *input.txt*. Essa entrada foi gerada com distâncias aleatórias para 40000 cidades, tamanho suficientemente grande para fazer as medidas. (Neste repositório também se encontra uma gerador de entrada aleatória para o programa, *generate_tsp_input.c*, caso seja necessário gerar outras entradas com tamanhos diferentes). O comando para rodar o programa é:

*./tsp < input.txt*

**Como medir o desempenho?**

O desempenho será medido pelo tempo de execução do programa. Seria interessantes fazer várias medidas para cada entrada. Por exemplo, para cada entrada, fazer 10 medidas, eliminar a mais alta e a mais baixa, e fazer a média das 8 restantes.

**Como apresentar o desempenho?**

Para melhor avalizar o TSP, foi realizada medidas em dois computadores:

1. Core intel i7, 8GB de Ram e HD SSD
2. Core intel i7, 8GB de Ram e HD de disco tradicional.

Tempo:

**Computador 1:**

Tempos de execução:

     241,580933167 seconds time elapsed
     370,233047456 seconds time elapsed
     185,747239262 seconds time elapsed
     188,915634975 seconds time elapsed
     186,388220851 seconds time elapsed
     186,553733052 seconds time elapsed
     184,304510746 seconds time elapsed
     185,860626947 seconds time elapsed
     188,714406583 seconds time elapsed
     183,985168797 seconds time elapsed

Tempo Médio:

    186,3086926516 seconds

removendo os dois valores discrepantes (241,580933167 e 370,233047456)

**Computador 2:**

     1253,672896651 seconds time elapsed
     154,135984080 seconds time elapsed
     154,421116130 seconds time elapsed
     155,355895240 seconds time elapsed
     154,159194934 seconds time elapsed
     153,774851966 seconds time elapsed
     154,728659218 seconds time elapsed
     153,961921142 seconds time elapsed
     153,942118491 seconds time elapsed
     154,449537507 seconds time elapsed

Tempo Médio:

    154,325475412 seconds

removendo o único valor discrepante (1253,672896651).

Outro dado que chamou bastante a atenção foi a troca de contexo do processador. A baixo será mostrado os resultados obtidos

**Computador 1**

                39.302      context-switches
                632.577     context-switches      
                488         context-switches       
                675         context-switches      
                684         context-switches       
                704         context-switches       
                425         context-switches       
                622         context-switches      
                700         context-switches       
                399         context-switches      

**Computador 2**

                30.342      context-switches       
                34.091      context-switches       
                34.249      context-switches       
                33.924      context-switches       
                34.308      context-switches       
                34.043      context-switches       
                34.494      context-switches       
                32.981      context-switches       
                33.435      context-switches       
                34.219      context-switches         

É possível notar como o computador 2 ficou no geral com poucas trocas de contexos em comparação do computador 1, que somenta a primeira execução teve trocas de ordem do computador 2.

Ciclos do processador também chamou a atenção, computador 1 teve média de ciclos de 571305707328,625 já o computador 2 teve média menor de 442410474359,8