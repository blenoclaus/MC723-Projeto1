#include<stdio.h>
#include<stdlib.h>
/*
 *  Fonte: https://www.thecrazyprogrammer.com/2017/05/travelling-salesman-problem.html
 */
 
int n, cost = 0;
int **ary = NULL;
int *completed = NULL;
int size_struct = 50000;

void init() {
    completed = malloc(size_struct * sizeof(int));
    ary = malloc(size_struct * sizeof(int*));
    int i;
    for (i = 0; i < size_struct; i++) {
        ary[i] = malloc(size_struct * sizeof(int));
    }
}
 
void takeInput() {
    int i,j;   
    scanf("%d",&n);
 
    for(i=0;i < n;i++) {
        for( j=0;j < n;j++) 
            scanf("%d",&ary[i][j]);
        completed[i]=0;
    }
}
 
void mincost(int city) {
    int i,ncity; 
    completed[city]=1;
 
    printf("%d--->",city+1);
    ncity=least(city);
 
    if(ncity == 999) {
        ncity = 0;
        printf("%d",ncity+1);
        cost+=ary[city][ncity]; 
        return;
    } 
    mincost(ncity);
}
 
int least(int c) {
    int i,nc = 999;
    int min = 999, kmin;
 
    for(i=0;i < n;i++) {
        if((ary[c][i]!=0) && (completed[i]==0))
            if(ary[c][i]+ary[i][c] < min) {
                min=ary[i][0]+ary[c][i];
                kmin=ary[c][i];
                nc=i;
            }
    }
 
    if(min!=999)
        cost+=kmin;
 
    return nc;
}
 
int main() {
    init();
    takeInput();
    printf("\n\nThe Path is:\n");
    mincost(0); 
    printf("\n\nMinimum cost is %d\n ",cost);
    return 0;
}
